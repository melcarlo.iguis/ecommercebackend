const mongoose = require('mongoose')
const productSchema = new mongoose.Schema({

	name : {
		type: String,
		required : [true, 'Name is required']
	},

	description : {
		type: String,
		required : [true, 'Description is required']
	},

	picture : {
		type: String,
		required : [true, 'picture is required']
	},

	cloudinary_id : {
		type: String,
		required : [true, 'cloudinary id is required']
	},

	category : {
		type: String,
		required : [true, 'Category is required']
	},


	price : {
		type: Number,
		required : [true, 'Price is required']
	},

	numberOfStock: {
		type: Number, 
		required : [true , 'Number of Stock is required']	
	},
	
	isActive : {
		type: Boolean,
		default : true
	},

	createdOn : {
		type: Date,
		default : new Date()
	}

})

module.exports = mongoose.model('Product', productSchema)